# Caméra RICOH THETA V

## Paramétrage de base
- installer l'application THETA sur le smartphone
- dans les paramètres wifi du smartphone, activer le réseau THETAYL**serialnumber**.OSC
- le mot de passe est **serialnumber**
- pour notre caméra :
    - **serialnumber** : 00188337    
    - **SSID** : THETAYL00188337.OSC
    - **Mot de passe** : 00188337

Quand l'application démarre, elle est en mode visualisation. Pour accéder au paramétrage de la caméra il faut cliquer sur l'icone en bas à gauche et cliquer sur [Paramètres]

## Mise à jour du firmware
- aller sur https://support.theta360.com/fr/download/
- installer l'application de base 
- connecter la caméra en USB
- dans l'application choisir [Fichier] puis [Mise à jour du micrologiciel...]

## Modes Wifi
### Paramétrage du mode Client
De base la caméra est en mode Access Point (AP). Dans ce mode, l'application est accessible à partir d'un smartphone uniquement.
Pour pouvoir y accéder à travers le réseau, il faut mettre la caméra en mode Client (CL)
Pour cela :
- allumer la caméra et se connecter en mode AP. L'icone Wifi s'allume en bleu
- lancer l'application sur le smartphone
- Appuyer sur [Paramètres]
- appuyer sur [Mode Client LAN sans fil]
- appuyer sur [Paramètres de point d'accès]
- appuyer sur [Ajouter] et saisir le SSID, le type de sécurité et le mot de passe du réseau
- appuyer sur [Paramètres d'authentification] et saisir un mot de passe. C'est ce mot de passe qui sera demandé pour se connecter à la caméra.

**Conseil** : mettre le même que le mot de passe du mode AP (**00188337**)

### Détermination de l'adresse IP de la caméra
En mode AP, l'adresse IP de la caméra est 192.168.1.1

Pour déterminer l'adresse IP en mode CL :
- mettre la caméra en mode Client Wifi
- lancer l'application sur le smartphone
- sur la page **Chronologie** cliquer sur l'icone [Img app photo]
- cliquer sur [Se connecter via le mode client LAN sans fil]
- cliquer sur [Paramètres]
- cliquer sur [Paramètres de l'appareil photo]
- cliquer sur [Version de l'appareil]

## Boutons et affichage
### Boutons
En plus du déclencheur, la caméra dispose de 3 boutons :
- Power : sert à allumer/éteindre la caméra. **ATTENTION** quand la caméra est allumée, un appui bref sur le bouton Power met la caméra en veille. Pour l'éteindre il faut faire un appui long. La led de status clignote alors en rouge.
- Wifi : sert à activer / désactiver le wifi ainsi qu'à changer de mode de connexion Wifi
- Mode : sert à changer de mode de prise de vue (photo / vidéo) et à démarrer / arrêter un plugin

### Affichage
La caméra affiche son status au moyen :
- d'une LED : 
    - quand elle est éteinte, la caméra est éteinte
    - quand elle clignote en bleu, la caméra démarre
    - quand elle est allumée en bleu, la caméra fonctionne
    - quand elle est allumée en blanc, le plugin est démarré
    - quand elle clignote en rouge, la caméra est en erreur
- d'une icone Wifi :
    - quand elle est éteinte, le wifi est désactivé
    - quand elle clignote en bleu, le wifi est en cours d'initialisation
    - quand elle est allumée en bleu, la caméra est en mode Access Point (AP)
    - quand elle est allumée en vert, la caméra est en mode Client (CL)
- d'une icone de Mode :
    - quand un appareil photo est affiché, la caméra est en mode Photo
    - quand une caméra est affiché, la caméra est en mode Vidéo
    - quand l'indication Live est affichée, la caméra est en mode Streaming

## Gestion des plugins
Pour ajouter des fonctionnalités à la caméra, il est possible d'utiliser des plugins. Le workflow d'utilisation des plugins est le suivant :
- installation
- activation
- démarrage

### Installation
- brancher la caméra en USB
- aller sur https://pluginstore.theta360.com
- choisir un plugin
- cliquer sur Installer
- l'application Ricoh THETA va s'ouvrir
- cliquer sur Ok pour installer le plugin
- le plugin s'installe

### Activation
La méthode la plus simple pour activer un plugin est par l'application smartphone.
- lancer l'application
- aller dans le paramétrage de l'appareil photo
- choisir [Paramètres de l'appareil photo]
- choisir [Plug-in]
- sélectionner la plugin à utiliser

### Démarrage
Pour démarrer le plugin, il faut appuyer 2 secondes sur le bouton Mode. La LED devient blanche ce qui signifie que le plugin est démarré.

Pour arrêter le plugin, appuyer à nouveau 2 secondes sur le bouton Mode.

## Streamer en RTMP
Ce mode de streaming envoie le flux vidéo vers un serveur RTMP. 

Dans notre cas, il s'agira de notre serveur NGINX.

### Configuration minimale du serveur NGINX
La configuration de ce serveur doit contenir au minimum :
````
rtmp {
    server {
        listen 1935;
        chunk_size 4096;
  	application stream {
	    live on;
	    record off;
	}
}
````

### Installation du plugin
- connecter la caméra en USB
- installer le plugin 'Wireless Live Streaming' (https://pluginstore.theta360.com/plugins/com.theta360.cloudstreaming/)
- activer le plugin au moyen de l'application smartphone
- démarrer le plugin en appuyant 2 secondes sur le bouton Mode

### Paramétrage du plugin
- mettre la caméra en mode Client Wifi
- se connecter sur http://**ip_de_la_camera**:8888
- renseigner les paramètres comme suit :
    - **Serveur URL** : rtmp://**adresse_serveur_nginx**:1935
    - **Stream Name/Key** : stream/theta
    - **Resolution** : 2K (1920x960)
- cliquer sur [Fix streaming settings]

### Utilisation du plugin
- cliquer sur [Start streaming] ou appuyer sur le déclencheur de la caméra
- visualiser le flux avec VLC, par exemple :
    - lancer VLC
    - cliquer sur [Fichier] puis [Ouvrir un flux réseau]
    - entrer l'adresse du flux : rtmp://**adresse_serveur_nginx**:1935/stream/theta
    - par exemple : rtmp://10.5.91.77:1935/stream/theta

## Streamer en RTSP
Ce mode de streaming lance un serveur de streaming sur la caméra.

### Installation du plugin
- connecter la caméra en USB
- installer le plugin 'THETA RTSP Streaming' (https://pluginstore.theta360.com/plugins/com.sciencearts.rtspstreaming/
)
- activer le plugin au moyen de l'application smartphone
- démarrer le plugin en appuyant 2 secondes sur le bouton Mode

### Paramétrage du plugin
Ce plugin n'a pas de paramétrage

### Utilisation du plugin
- appuyer sur le déclencheur de la caméra
- visualiser le flux avec VLC, par exemple :
    - lancer VLC
    - cliquer sur [Fichier] puis [Ouvrir un flux réseau]
    - entrer l'adresse du flux : rtsp://**adresse_camerax**:8554/live?resolution=1920x960
    - par exemple : rtsp://10.5.91.75:8554/live?resolution=1920x960

## OpenCV
### Référence
https://docs.opencv.org/2.4/modules/refman.html

Documetation OpenCV à compléter.

## Liens intéressants :
https://video.stackexchange.com/questions/23405/vlc-3-0-vetinari-unable-to-play-360-videos

http://paulbourke.net/geometry/transformationprojection/

http://paulbourke.net/miscellaneous/sphere2sphere/

https://github.com/fuenwang/Equirec2Perspec/blob/master/README.md

https://github.com/oaubert/python-vlc/blob/81926faa7bdefe3cdd01d64476154191de8c3702/generated/3.0/vlc.py#L7128

https://www.google.com/search?q=360+video+player&oq=360+video+play&aqs=chrome.1.69i57j0l3.11305j0j4&client=ms-android-samsung-ss&sourceid=chrome-mobile&ie=UTF-8

https://community.theta360.guide/t/theta-v-360-is-output-dual-fisheye-not-equirectangular/1807/4

https://stackoverflow.com/questions/43741885/how-to-convert-spherical-coordinates-to-equirectangular-projection-coordinates

https://github.com/robbykraft/Panorama

http://paul-reed.co.uk/programming.html

## Nouveaux liens (11/06/2020)

https://community.theta360.guide/t/low-latency-0-4s-h-264-livestreaming-from-theta-v-wifi-to-html5-clients-with-rtsp-plug-in-ffmpeg-and-janus-gateway-on-raspberry-pi/4887

https://community.theta360.guide/t/successful-theta-v-stream-from-drone-to-vr-headset-0-25-miles-away/4437/30

https://community.theta360.guide/t/extended-livepreview-sample-code-for-theta-plug-in-with-webui/5272

