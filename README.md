# Tests sur caméra RICOH THETA V

Voilà ce qui a été testé :

- Mise en marche : OK
-	Mise à niveau firmware : OK
-	Installation et compréhension du fonctionnement de l’appli smartphone : OK
-	Installation et compréhension du fonctionnement de l’appli PC : OK
-	Compréhension des différents modes de fonctionnement : OK
-	Fonctionnement simultané du wifi et de l’USB pour la charge : OK
-	Activation du mode CLIENT pour le Wifi : OK
-	Installation du plugin RTMP : OK
- Fonctionnement du plugin RTMP :
   -  Paramétrage du plugin : OK
   -	Activation du plugin : OK
   -	Démarrage du plugin : OK
   -	Lancement d’un streaming vers le serveur NGINX : OK
   -	Visualisation du flux avec VLC : OK (pas toujours)
   -	Visualisation du flux avec OBS : OK
   -	Enregistrement du flux : OK
-	Installation du plugin RTSP : OK
- Fonctionnement du plugin RTSP : (pas de paramétrage)
   -	Activation du plugin : OK
   -	Démarrage du plugin : OK
   -	Visualisation du flux avec VLC : OK
   -	Visualisation du flux avec OBS : OK
   -	Enregistrement du flux : OK
- Prise en main de la librairie OpenCV pour faire du traitement d’image :
   -	Compilation de la dernière version : OK
   -	Affichage du flux RTMP ou RTSP : OK (projection equirectangulaire)
   -	Rectification de l’image du flux RTMP ou RTSP : OK
   -	Ajout d’information dans l’image d’un flux RTMP ou RTSP : OK
- Prise en main de l’API :
   -	Affiche des informations de la caméra et de son status : OK
   -	Activation / Désactivation d’un plugin : OK
   -	Démarrage d’un plugin : OK
   -	Arrêt d’un plugin : NOK

Tout fonctionne à peu près correctement. Le flux RTMP est plus performant que le flux RTSP.
Le problème que j’ai avec l’arrêt d’un plugin n’est pas rédhibitoire 

J’ai commencé à documenter tout ce que j’ai fait. Pour l’instant c’est juste un draft. Il manque des copies d’écran, des photos de la caméra pour montrer les boutons et les leds d’état. Voir doc en pj.

Pour tester OpenCV, j’ai écrit un programme python : lire_flux.py. 
Ce programme peut streamer un flux RTMP ou RTSP. En option, il peut rectifier l’image pour afficher une image à plat plus facilement lisible.

Pour tester l’API, j’ai écrit un autre programme python : theta_plugin.py.
Ce programme peut afficher différentes informations relatives à la caméra et son status. Il peut également activer ou désactiver un plugin et démarrer un plugin. 
L’arrêt ne fonctionne pas toujours mais il n’est pas nécessaire.

**Voir plus d'informations dans [README-detaille.md](README-detaille.md)**
