"""
theta_plugin.py : utilisation de l'API pour controler les plugins.

Based on http://lists.theta360.guide/t/developing-theta-client-mode-applications/2450

This script uses Python modules requests
      $ pip install requests

Prerequisite :
    Install RTMP (com.theta360.cloudstreaming) and RTSP (com.sciencearts.rtspstreaming) plugins

Usage :
  --info                show active plugin
  --plugin {rtmp,rtsp}  activate rtmp or rtsp plugin
  --action {start,stop} start or stop active plugin
"""

import requests
from requests.auth import HTTPDigestAuth
import argparse
import sys

# global constants specific to your THETA. Change for your camera.
THETA_ID = 'THETAYLxxxxxxxx'
THETA_PASSWORD = 'xxxxxxxx'  # default password. may have been changed
THETA_URL = 'http://xx.xx.xx.xx/osc/'

PLUGINS = [
        {'name': 'rtmp', 'packageName':'com.theta360.cloudstreaming'},
        {'name': 'rtsp', 'packageName':'com.sciencearts.rtspstreaming'}
        ]

def thetaExecute(payload):
    url = THETA_URL + 'commands/execute'
    resp = requests.post(url,
                        json=payload,
                        auth=(HTTPDigestAuth(THETA_ID, THETA_PASSWORD)))
    #print(resp.status_code)
    return resp


def activePlugin():
    payload = {"name": "camera._listPlugins"}
    resp = thetaExecute(payload)
    try:
        plugins=resp.json()['results']['plugins']
    except:
        print("Error : ", resp.status_code)
        exit(1)

    activePlugin = next(item for item in plugins if item['boot'] == True)
    packageName = activePlugin['packageName']
    try:
        plugin=next(item for item in PLUGINS if item['packageName'] == packageName)
        return plugin['name']
    except StopIteration:
        return 'None'


def showActive():
    active = activePlugin()
    if active == 'None':
        print('No streaming plugin active')
    else:
        print('Active plugin : ', active)


def activatePlugin(stream):
    plugin=next(item for item in PLUGINS if item['name']==stream)
    packageName=plugin['packageName']

    payload = {"name": "camera._setPlugin", "parameters": {"packageName": packageName, "boot": True}}
    try:
        resp = thetaExecute(payload)
    except:
        print("Error : ", resp.status_code)
        exit(1)

    showActive()


def controlPlugin(ctrl):
    comment = "Start" if ctrl == "start" else "Stop"
    action = "boot" if ctrl == "start" else "finish"

    payload = {"name": "camera._pluginControl", "parameters": {"action": action}}
    try:
        resp = thetaExecute(payload)
    except:
        print("CTRL Error : ", resp.status_code)
        exit(1)

    if resp.status_code == 200:
        active = activePlugin()
        print(active, comment, ": OK")
    else:
        print(comment, ": Error ", resp.status_code)


parser=argparse.ArgumentParser()

parser.add_argument('--show', help='show active plugin', action='store_true')
parser.add_argument('--plugin', choices=['rtmp', 'rtsp'], help='activate rtmp or rtsp plugin', action='store')
parser.add_argument('--action', choices=['start', 'stop'], help='start or stop active plugin', action='store')

args=parser.parse_args()

if len(sys.argv) < 2:
    parser.print_usage()
elif args.show:
    showActive()
elif args.plugin != None:
    activatePlugin(args.plugin)
elif args.action != None:
    controlPlugin(args.action)
