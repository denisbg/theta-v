"""
theta_rtmp.py : streaming RTMP via OpenCV
"""
import cv2
import numpy as np

cap=cv2.VideoCapture('rtmp://10.5.91.74/hls/live', cv2.CAP_FFMPEG)
while True:
    ret, frame=cap.read()
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF==ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
