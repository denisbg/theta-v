"""
theta_rtsp.py : accès à un flux RTSP via OpenCV
"""
import cv2
import numpy as np

#cap=cv2.VideoCapture('rtsp://10.5.91.75:8554/live?resolution=1024x512', cv2.CAP_FFMPEG)
cap=cv2.VideoCapture('rtsp://10.5.91.75:8554/live')
while True:
    ret, frame=cap.read()
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF==ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
