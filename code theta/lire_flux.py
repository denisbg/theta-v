"""
lire_flux.py : utilisation d'OpenCV pour accéder aux images de la caméra
"""
import cv2
import numpy as np
import os
import argparse
import sys
import pprint
import Equirec2Perspec as E2P 

parser=argparse.ArgumentParser()

parser.add_argument('--rect', help='Rectifie l\'image', action='store_true')
parser.add_argument('--stream', choices=['rtmp', 'rtsp'], help='choix du flux à streamer', action='store', required=True)

args=parser.parse_args()
pprint.pprint(args)

if args.stream == 'rtmp':
    stream_url='rtmp://10.5.91.77:1935/stream/theta'
    cap=cv2.VideoCapture(stream_url)
else    :
    os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"]="rtsp_transport;udp"
    stream_url='rtsp://10.5.91.75:8554/live?resolution=1024x512'
    cap=cv2.VideoCapture(stream_url, cv2.CAP_FFMPEG)

while True:
    ret, frame=cap.read()
    if args.stream == 'rtmp':
        image = frame
    else:
        image = cv2.flip(frame,1)

    if args.rect:
        equi = E2P.Equirectangular(image)
        cv2.imshow('Image rectifiée', equi.GetPerspective(90, 0, 0, 400, 600))
    else:
        cv2.imshow('Image Brute', image)

    if cv2.waitKey(1) & 0xFF==ord('q'):
        break
cap.release()
cv2.destroyAllWindows()

