# Exemples de code pour THETA V

ATTENTION : pas mal d'infos en dur dans les scripts

## [theta_plugin.py](theta_plugin.py)
Script permettant de manipuler les plugins RTMP et RTSP via l'API de la caméra.

Source : [https://community.theta360.guide/t/tip-developing-theta-client-mode-applications/2450](https://community.theta360.guide/t/tip-developing-theta-client-mode-applications/2450)

## [lire_flux.py](lire_flux.py)
Utilisation d'OpenCV pour récupérer un flux en provenance de la caméra (RTMP ou RTSP).

Le flux doit d'abord avoir été lancé soit manuellement, soit par le biais de [lire_flux.py](lire_flux.py)

Ce programme utilise [Equirec2Perspec.py](Equirec2Perspec.py) pour aplanir l'image à 360° issue de la caméra

## [theta_rtmp.py](theta_rtmp.py)
Test unitaire d'OpenCV pour lire un flux RTMP

## [theta_rtsp.py](theta_rtsp.py)
Test unitaire d'OpenCV pour récupérer un flux RTSP
